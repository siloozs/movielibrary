import React, { Component } from 'react';
import './App.css';
import 'core-js/fn/array/find';
import 'core-js/fn/array/every';
import ModalWrapper from './Components/Modal';
import MsgWrapper from './Components/error';
import Lang from './Lang';
import Utils from './Utlis';
import axios from 'axios';
import Loader from './Components/Loader';
import Movie from './Components/Movie';

import HeaderWrapper from './Components/Header';


class App extends Component {
  constructor(props){
    super(props);
    this.showpopup = this.showpopup.bind(this);
    this.addmovie = this.addmovie.bind(this);
    this.savechanges = this.savechanges.bind(this);
    this.closeModal = this.closeModal.bind(this);

    this.delete = this.delete.bind(this);
    this.edit = this.edit.bind(this);
    this.state = {
          movies:[],
          modal:false,
          isLoading: true,
          error:false,
          finish: false,
          formValid: true,
          success: false,
          action:Lang.addText,
          title:'',
          idx:0,
          newMovie:{
            title: '',
            year:'',
            runtime:'',
            genere: '',
            director: ''
          }
    }

  }

  closeModal(){
      this.setState({
        modal:false,
        success:false
      });
  }

// update the selection of the movie to be displayed.
  edit(idx){
      const currentMovie = this.state.movies[idx];
      this.setState({
        title: currentMovie.Title,
        action:Lang.editText,
        idx:idx,
        modal: true,
        formValid: true,
        newMovie:{
          title:  currentMovie.Title,
          year: currentMovie.Year,
          runtime: window.parseInt(currentMovie.Runtime),
          genere: currentMovie.Genre,
          director: currentMovie.Director
        }
      });
     
    }

  delete(idx){
    const data = this.state.movies.slice(); // create shallow copy
    data.splice(idx , 1); // remove the item
    // update the state
    this.setState({
      movies:data
    });
}

  componentDidMount(){
    axios.get('./data.json')
    .then( result => {
      this.setState({ 
      movies: result.data.map((el , idx) => Utils.filter(el, idx) ),
      isLoading: false,
      finish: true
    }) }
    )
    .catch(error => this.setState({
      error,
      isLoading: false
    }));
  }

  showpopup(e){
    e.preventDefault();
    this.setState({
      action:Lang.addText,
      formValid: true,
      modal: true,
      success:false,
      newMovie:{
        title: '',
        year:'',
        runtime:'',
        genere: '',
        director: ''
      }
    });
  }

  addmovie(e){

     const {newMovie , formValid , action , idx} = this.state;
     const bEmptyKey = Object.keys(newMovie).every((k) => newMovie[k] !== "");
    
     if(!bEmptyKey){
      this.setState({
        errorMessage: Lang.errors.emptyfrom,
        formValid: false
      });

     }else if(formValid === true && action === Lang.addText){
          //  check that such name does not already exists
          const aMovies = [...this.state.movies];
          const TitleTocheck = Utils.formatTitle(newMovie.title);
          const isTitleExists =  Utils.checkfordups(aMovies,  TitleTocheck );
          if(isTitleExists === true){ 
            this.setState({
              errorMessage: Lang.errors.titleExists,
              formValid: false
            });
          }else{
                let oMovieOject = {
                  Index: aMovies.length,
                  Year: newMovie.year,
                  Title: TitleTocheck,
                  Runtime: newMovie.runtime + Lang.min,
                  Genre: newMovie.genere,
                  Director: newMovie.director,
                  Poster: Utils.DefaultPoster + "?text=" + TitleTocheck
              }
              aMovies.push(oMovieOject);
              this.setState({
                movies:aMovies,
                success: true,
                errorMessage: Lang.success.add
              });
          }
        
     }else if( formValid === true && action === Lang.editText){
        //  update the idx 
        const aMovies = [...this.state.movies];
        const TitleTocheck = Utils.formatTitle(newMovie.title);
        const isTitleExists =  Utils.checkfordups(aMovies,  TitleTocheck , idx);
        if(isTitleExists === false){
        aMovies[idx].Year =  newMovie.year;
        aMovies[idx].Title = TitleTocheck;
        aMovies[idx].Runtime =  newMovie.runtime  + Lang.min;
        aMovies[idx].Genre = newMovie.genere;
        aMovies[idx].Director = newMovie.director;
        if(aMovies[idx].Poster.indexOf(Utils.DefaultPoster) > -1 ){
          aMovies[idx].Poster = Utils.DefaultPoster + "?text=" + newMovie.title;
        }
        this.setState({
            movies: aMovies,
            success: true,
            errorMessage: Lang.success.edit
        });  

        }else{

          this.setState({
            errorMessage: Lang.errors.titleExists,
            formValid: false
          });
        }
        
        
     }
    
  } 

  savechanges(e){
    const name = e.target.name;
    const value = e.target.value;
    let oMovie  = {...this.state.newMovie};
    let oFormInput = { formValid: true };
    const { action , title , movies , idx } = this.state;
 
    if(name === "title" && action === "edit" && value !== title && value !== ""){
        const found = Utils.checkfordups(movies , value , idx);
        if(found === true){
          oFormInput.formValid = false;
          oFormInput.errorMessage = Lang.errors.titleExists;
        }
    }else{
        oFormInput = Utils.validateForm(value , name); 
    }
    
    oMovie[name] = value;  
    this.setState({
      success: false,
      newMovie: oMovie,
      errorMessage: oFormInput.errorMessage,
      formValid: oFormInput.formValid
    });

  }

  render() {
    const { formValid , errorMessage , isLoading , movies , error , modal , action , success} = this.state;
    const TitleMsg = action === Lang.addText ? Lang.add : Lang.save;
    
    let ErrorMsg = null;
    if(formValid === false){
      ErrorMsg =  <MsgWrapper text={errorMessage}  type="alert-danger" /> ;
    }else if(success === true && errorMessage !== ''){
      ErrorMsg =  <MsgWrapper text={errorMessage}  type="alert-success" /> ;
    } 
      
    
    if(isLoading === true){
        return (<Loader />);
    }else if(movies.length > 0  &&  error === false){

      return (
        <div className="App">
        <HeaderWrapper showpopup={this.showpopup} />
        <div className="container">
            <ModalWrapper error={ErrorMsg} close={this.closeModal} value={this.state.newMovie} visible={modal} addmovie={this.addmovie} actiontext={TitleMsg} save={this.savechanges}/>
           <div className="card-columns">
            {
              movies.map( (movie ,idx) => (
                    <Movie key={idx} index={idx} movie={movie}  delete={this.delete} edit={this.edit} />
              ))
            }
            </div>
        </div>
      </div>
      ); 

    }else if(movies.length === 0 ){

      return (
        <div className="App">
        <HeaderWrapper showpopup={this.showpopup} />
        <div className="container">
        <ModalWrapper error={ErrorMsg} close={this.closeModal} value={this.state.newMovie}  visible={modal} addmovie={this.addmovie} actiontext={TitleMsg} save={this.savechanges}/>
           <MsgWrapper text={Lang.errors.emptyMovies}  type="alert-danger"/>
        </div>
      </div>
      );
    }
    
  }
}

export default App;
