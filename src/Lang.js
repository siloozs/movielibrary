
const Lang = {
    logo: "Movie Library",
    errors:{
          title: 'Please Enter Title',
          year: 'The year is invalid , must be a number and above 1920',
          runtime: 'The runtime is invalid , must be  a number',
          genere: 'The Genere can not be empty',
          director: 'Director can not be empty',
          emptyMovies: 'Library is empty , no movies to display',
          titleExists: 'A movie with the same title already exists',
          emptyfrom: "Please fill the form, all fields are mandatory"
    },
    success:{
       edit: "Changes have been saved successfully",
       add: "The new movie has been added!"
    },
    add: 'Add new movie',
    save:'Edit Movie',
    addText: 'add',
    editText: 'edit',
    min: " min"

}

export default Lang;