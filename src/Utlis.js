import Lang from './Lang';
import 'core-js/fn/array/find';



const Utils = {
    DefaultPoster: 'https://via.placeholder.com/314X466',
    API:{
          url:'http://www.omdbapi.com/?apikey=b5f0c5c6&',
          query: 's=2017'
    },
    formatTitle: function(title){
       return ( title.charAt(0).toUpperCase() + title.slice(1).toLowerCase() ).replace(/[^a-z\s0-9]/gi, ''); 
    },
    filter: function(oMovie , index){
      oMovie.Title = this.formatTitle(oMovie.Title);//( oMovie.Title.charAt(0).toUpperCase() + oMovie.Title.slice(1).toLowerCase() ).replace(/[^a-z\s0-9]/gi, '');
      oMovie.Index = index + 1;
      return oMovie;
    },
    checkfordups:function(aMovies , title , excludeIdx){
        // check if such Title already exists in the Library
      const found = aMovies.find( (elem , idx) => {  
        return excludeIdx !== undefined ? (elem.Title === title & idx !== excludeIdx) : elem.Title === title;
      });
        return found !== undefined;
    },
    validateForm: function(value , name){
      
      let formValid = true;
      let errorMessage = "";

      switch(name){
        case 'title':
            if(value === ""){
              formValid = false; 
              errorMessage = Lang.errors.title;
            }
        break;
        case 'year':
            let year = window.parseInt(value);
            let current_year = new Date().getFullYear();
            let regyear = /^[0-9]+$/;
            if( !regyear.test(year) || year > current_year || value.length !== 4 || year < 1920){
              errorMessage = Lang.errors.year;
              formValid = false;
            }
  
        break;
        case 'runtime':
            let runtime = window.parseInt(value);
            if(window.isNaN(runtime) || runtime < 1){
              errorMessage = Lang.errors.runtime;
              formValid = false;
            }
        break;
        case 'genere':
            if(value === ""){
              errorMessage = Lang.errors.genere;
              formValid = false;
            }
        break;
        case 'director':
              if(value === ""){
                  errorMessage = Lang.errors.director;
                  formValid = false;
              }
        break;
        default:
        
        break;
  
      }

        return {
          errorMessage: errorMessage,
          formValid: formValid
        }
     }
}

export default Utils;