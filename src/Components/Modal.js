import React from 'react';
import Modal from 'react-bootstrap4-modal';

const ModalWrapper = (props) =>{
  return (
    <Modal visible={props.visible} >
    <div className="modal-header">
      <h5 className="modal-title">{props.actiontext}</h5>
      <button onClick={props.close} type="button" className="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div className="modal-body">
    <div className="form-group text-left">
              <label htmlFor="title">Title</label>
              <input className="form-control" value={props.value.title} type="text" name="title" id="title"  onChange={props.save}/>
        </div>
        <div className="form-group text-left">
              <label htmlFor="year">Year</label>
              <input className="form-control" value={props.value.year} type="text" name="year" id="year" onChange={props.save}/>
        </div>
        <div className="form-group text-left">
              <label htmlFor="runtime">Runtime(Min)</label>
              <input className="form-control" value={props.value.runtime} type="text" name="runtime" id="runtime" onChange={props.save}/>
        </div>
        <div className="form-group text-left">
              <label htmlFor="genere">Genere</label>
              <input className="form-control" value={props.value.genere}  type="text" name="genere" id="genere" onChange={props.save}/>
        </div>
        <div className="form-group text-left">
              <label htmlFor="director">Director</label>
              <input className="form-control" value={props.value.director} type="text" name="director" id="director"  onChange={props.save}/>
        </div>
    </div>
    <div className="modal-footer">
      <button onClick={props.close} type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
      <button onClick={props.addmovie} type="button" className="btn btn-primary">{props.actiontext}</button>
    </div>
    {props.error}
  </Modal>
  );


}


export default ModalWrapper;

// export default Modal;