import React, { Component } from 'react';


class EditBtn extends Component {
  handleClick = (e) => {
    e.preventDefault();
    this.props.edit(this.props.value);
  }

  render() {
    return (
       <a href="" onClick={this.handleClick} className="btn btn-primary">Edit</a>
    );
  }
}

export default EditBtn;