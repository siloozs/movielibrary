import React from 'react';

const MsgWrapper = (props) =>(
  <div className={props.type + " alert alert-dismissible fade show mx-auto alert-wrapper"} role="alert">
                {props.text}
  </div>
);


export default MsgWrapper;