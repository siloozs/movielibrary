import React from 'react';


const Loader = () =>(
  <div className="loading-wrapper">
            <div id="loading">
                <ul className="bokeh">
                    <li></li>
                    <li></li>
                    <li></li>
                </ul>
            </div>
          </div>
);

export default Loader;