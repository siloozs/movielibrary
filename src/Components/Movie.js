import React from 'react';
import DeleteBtn from './BtnDelete';
import EditBtn from './EditBtn';

import { Card, CardImg, CardBody,
      CardHeader, CardFooter} from 'reactstrap';


const Movie = (props) =>(
    <div>
    <Card>
        <CardHeader className="bg-transparent">
            <h5 className="card-title">{props.movie.Title}</h5>
        </CardHeader>  
      <CardBody>
        <CardImg top src={props.movie.Poster} alt={props.movie.Title} />
        <div className="pt-1">
         <div className="text-left"><strong>Year:</strong> {props.movie.Year}</div>
         <div className="text-left"><strong>Director:</strong> {props.movie.Director}</div>
         <div className="text-left"><strong>Runtime:</strong> {props.movie.Runtime}</div>
         <div className="text-left"><strong>Genre:</strong> {props.movie.Genre}</div>                   
         </div>
      </CardBody>
      <CardFooter>
            <EditBtn edit={props.edit} value={props.index} />
            <DeleteBtn delete={props.delete} value={props.index} />
      </CardFooter>
    </Card>
  </div>      
);




 export default Movie;