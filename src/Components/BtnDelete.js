import React, { Component } from 'react';


class DeleteBtn extends Component {
  handleClick = (e) => {
    e.preventDefault();
    this.props.delete(this.props.value);
  }

  render() {
    return (
       <a href="" onClick={this.handleClick} className="btn btn-danger">Delete</a>
    );
  }
}

export default DeleteBtn;